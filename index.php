<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bands In Town - Artist Events</title>

		<style>
		
			html,
			button,
			input,
			select,
			textarea {
			    font-family: sans-serif;
			    background: #999;
			    font-size: 13px;
			    line-height: 18px;
			}
		
			#wrapper {
				width: 960px;
				margin: 0 auto;
			}
			
			h1 {
				font-size: 32px;
			}
		
			#events {
				width: 960px;
				height: 400px;
				margin: 0 auto;
			}
		
			.event {
				float: left;
				clear: left;
				width: 100%;
			}
			
			.event p {
				margin: 0px;
			}
			
			.event .event-date {
				float: left;
				width: 120px;
			}
			
			.event .event-name {
				float: left;
				color: white;
				width: 320px;
			}
			
			.event .event-location {
				float: left;
				color: white;
				width: 200px;
			}
			
			.event .event-tickets {
				float: left;
				color: white;
				width: 100px;
			}
			
				.event .event-tickets a {
					color: white;
				}
			
			.event .event-rsvp, .event .event-share {
				width: 40px;
				float: left;
			}
				
			.event .event-rsvp a, .event .event-share a {
				color: #484057;
			}
		
		</style>
    </head>
    <body>
    	<div id="wrapper">
	    	
	    	<h1>Bands In Town - Artist Events with PHP & JSON</h1>
	    	
		    <div id="events">
		    <?php
				//Set JSON feed artist
				$artistId = 'Nicky%20Romero'; //Set artist ID
				$jsonData = file_get_contents("http://api.bandsintown.com/artists/". $artistId ."/events.json");
				$phpArray = json_decode($jsonData, true);
				
				$output = '';
				
				$i = 0;
				foreach ($phpArray as $key => $value) {
					
					// Max. amount of events, set -1 for all
					$amountShown = -1;
					
					if($i == $amountShown){  
						break;
					}
					
					// Set time in usable format
					sscanf($value['datetime'],"%u-%u-%uT%u:%u:%uZ",$year,$month,$day,$hour,$min,$sec);
					$newtstamp = mktime($hour,$min,$sec,$month,$day,$year);
					
					// Set variables
					$eventDate = gmstrftime("%B %d",$newtstamp);
					$venueName = $value['venue']['name'];
					$venueCity = $value['venue']['city'];
					$venueCountry = $value['venue']['country'];
					$ticketStatus = $value['ticket_status'];
					$ticketUrl = $value['ticket_url'];
					$eventUrl = $value['url'];
					
					if ($ticketStatus == 'available') {
					    $ticketLink = '<a href="'. $ticketUrl .'" target="_blank">Tickets</a>';
				    }
					
					$output .= '
			            <div class="event">
			                <span class="event-date">
			                	<p class="time">'. $eventDate .'</p>
			                </span>
			                
			                <span class="event-name">
			                	<p class="name">'. $venueName .'</p>
			                </span>
			                
			                <span class="event-location">
			                	<p class="venue">'. $venueCity . ', '. $venueCountry . '</p>
			                </span>
			                
			                <span class="event-tickets">
								'. $ticketLink .'
			                </span>
			                
			                <span class="event-rsvp">
				                <a href="'. $eventUrl .'" target="_blank">RSVP</a>
			                </span>
			                
			                <span class="event-share">
				                <a href="http://www.facebook.com/sharer.php?u='. $eventUrl .'" target="_blank">Share</a>
			                </span>
			                
			            </div>
			        ';
				    
				    $i++;
				}
				
				echo $output;
				
			?>
		    </div>
    	</div>
    </body>
</html>